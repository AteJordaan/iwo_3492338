#!/bin/bash

#error message if no argument is provided
if [ -z $1 ]
then 
    echo "usage: ./script.sh FILE"
    exit
fi

FREQ="$(grep -Eo '\w+' $1 | grep -wi 'de' | wc -l)"
echo "$FREQ occurences of 'de' in file $1"
